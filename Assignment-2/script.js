// Quicksort Logics


class Person {
    constructor(name, age, salary, sex) {
        this.name = name;  this.age = age;
        this.salary = salary; this.sex = sex;
    }
    static sort(arr, field, order) {
        const temp = arr;
        this.quickSort(temp, field, order, 0, temp.length - 1);
        return temp;
    }
    static quickSort(arr, field, order, start, end) {
        if (start > end) {
            return;
        }
        const pivot = arr[end][field];
        let left = start - 1;
        for (let i = start; i <= end - 1; i++) {
            if (order == 'asc') {
                if (arr[i][field] < pivot) {
                    left++;
                    this.swap(arr, left, i);
                }
            } else if (order == 'desc') {
                if (arr[i][field] > pivot) {
                    left++;
                    this.swap(arr, left, i);
                }
            }
        }
        this.swap(arr, left + 1, end);
        this.quickSort(arr, field, order, start, left);
        this.quickSort(arr, field, order, left + 2, end);
    }
    static swap(arr, i, j) {
        const temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
const obj1 = new Person("deepanshu", 21, 100, "male");
const obj2 = new Person("Batra", 24, 500, "male");
const obj3 = new Person("Aish", 21, 50, "female");
const obj4 = new Person("Bharat", 22, 200, "male");
const obj = [obj1, obj2,obj3,obj4];



// const user=document.getElementById("name")
// const age=document.getElementById("age")
// const salary=document.getElementById("salary")
// const btn=document.getElementById("btn")


// btn.addEventListener('click',function(){
//     if(user.value && age.value && salary.value){
//         obj.push(new Person(user.value,age.value,salary.value,"male"))
//     }
//     console.log(Person.sort(obj, 'name', 'asc'))
// })  
    

console.log(Person.sort(obj, 'name', 'desc'))