const validateEmail = (email) => {
  const emailFormatValidator =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  if (
    email === null ||
    email.trim() === "" ||
    !email.trim().match(emailFormatValidator)
  ) {
    return false;
  }
  return true;
};

const validatePassword = (password) => {
  const passwordFormatValidator = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;

  if (
    password === null ||
    password.trim() === "" ||
    !password.trim().match(passwordFormatValidator)
  ) {
    return false;
  }

  return true;
};

const generateErrorMessage = (email, password, sex, role, permission) => {
  if (!email) return "Please enter valid email";
  if (!password)
    return "Password should be of min 6 characters with MIX of uppercase, lowercase and digits";
  if (!sex) return "Please select sex";
  if (!role) return "Please select your role";
  if (!permission) return "Please select atleast 2 permissions";
  return "Form has some error, please try again";
};

const validateForm = () => {
  const emailField = document.querySelector("#email__field");
  const passwordField = document.querySelector("#password__field");
  const sexDropdown = document.querySelector("#sex__dropdown");
  const role = document.querySelectorAll('input[name="role"]:checked');
  const permissions = document.querySelectorAll(
    'input[name="permission"]:checked'
  );

  const isEmailValidated = validateEmail(emailField.value);
  const isPasswordValidated = validatePassword(passwordField.value);
  const isSexSelected = sexDropdown.value !== "none";
  const isRoleSelected = role.length !== 0;
  const arePermissionsSelected = permissions.length >= 2;

  if (
    isEmailValidated &&
    isPasswordValidated &&
    isSexSelected &&
    isRoleSelected &&
    arePermissionsSelected
  ) {
    // hide form and submit button and shows the confirmation dialog
    let dialogData = `Email: ${emailField.value}\nSex: ${sexDropdown.value}\nRole: ${role[0]?.value}\n\nPermissions:\n`;
    for (let i = 0; i < permissions.length; i++) {
      dialogData += permissions[i].value + "\n";
    }
    document.querySelector("#form").style.display = "none";
    swal("Form validated", dialogData, "success");
  } else {
    swal(
      "Form error",
      generateErrorMessage(
        isEmailValidated,
        isPasswordValidated,
        isSexSelected,
        isRoleSelected,
        arePermissionsSelected
      ),
      "error"
    );
  }
};
